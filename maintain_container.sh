#!/bin/bash

#remove existing images and containers
sudo docker container stop emailer
sudo docker container rm emailer
sudo docker image rm tim/emailer

#build the new image and make a new container
sudo docker build -t tim/emailer .
sudo docker run -d --name emailer --restart always tim/emailer
