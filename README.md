This is a basic emailer primarily for downloading weather and advising clothing for bike rides to work.

Run emailer.py with --usage flag to see what options I've made available so far.

**Scheduling**
- Uses schedule package to run every day at *sendTime*.


**Weather Data**
- Pull from the weather API and build a dictionary for reference outside the sub-function.
- Refers to temperature limits and a clothing dictionary to make recommendations on clothing. These are hard-coded and are not adaptive.


**Email content**
- A string is built by concatenating dumb text and variables from the weather Data dictionary.
- MIMEMultipart is used to allow sending of HTML emails.

**Usage**
- On server where container already exists run maintain_container.sh: <br />
```
./maintain_container.sh
```

- Building yourself locally <br />
Run as container by cloning to a directory, adding the secrets file and run: <br />
```
sudo docker build -t <image-id> .
sudo docker images
sudo docker run -d --name <container-id> <image-id>
sudo docker exec -it <container-id> bash
```
