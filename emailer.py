import requests
import math
import schedule
import time
import smtplib
import ssl
import sys

from datetime import datetime
from datetime import date
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import secret

# first light calculation DONE
# some crypto stuff, binance market data endpoints, nicehash miner endpoints
# kNN for clothing suggestions

sendTime = "07:00"

# smtp_ssl configuration
port = 465
context = ssl.create_default_context()
receiver_email = "timothy.maltby@gmail.com"

# token = '1338596914:AAH6fe__rEYFvGLZXEk48al2lp043CvT-zw'
urlWeather = 'http://api.openweathermap.org/data/2.5/weather?q=Melbourne,au&units=metric&appid='
urlWeather += secret.apiKeyWeather
urlDogs = 'https://random.dog/woof.json'
urlAdvice = 'https://api.adviceslip.com/advice'

ATbins = [5, 9, 15]  # thresholds for binning temperatures
clothing = {         # clothing for each condition
    True: [
        "Jumper, breaker, thermal and gloves",
        "Jumper, breaker and gloves",
        "Breaker only",
        "hot af"
    ],
    False: [
        "Jumper, breaker, thermal and gloves",
        "Jumper, breaker and gloves",
        "Jumper only",
        "hot af"
    ]
}


def control():
    help = '''--help: view command information.
--send: immediately send email, overriding the schedule.
--time: report scheduled send time.
--gather: run API calls and print data array.
--health: run docker health check.
--server_time: report the time the server observes.
--finance: testing for finance information.'''

    if len(sys.argv) == 1:
        return

    if str(sys.argv[1]) == "--send":
        send_email()
        sys.exit()

    if str(sys.argv[1]) == "--gather":
        data = dataGathering()
        print(data)
        sys.exit()

    if str(sys.argv[1]) == "--finance":
        data = financeGathering()
        print(data)
        sys.exit()

    if str(sys.argv[1]) == "--help":
        print(help)
        sys.exit()

    if str(sys.argv[1]) == "--health":
        filename = "status_file.txt"
        with open(filename) as f:
            content = f.readlines()
            if content[0] == "Fail":
                print("Failed.")
                sys.exit(1)
            elif content[0] == "Success":
                print("Healthy.")
                sys.exit(0)
            else:
                sys.exit(1)

    if str(sys.argv[1]) == "--time":
        print("Send time is set to", sendTime)
        sys.exit()

    if str(sys.argv[1]) == "--server_time":
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print("Current server time is: ", current_time)
        sys.exit()


def binning(binbit, checkbits):
    for i in range(0, len(checkbits)):
        if binbit < checkbits[i]:
            return i
    return len(ATbins)


def dataGathering():
    # weather data

    resp = requests.get(urlWeather)
    JSONweather = resp.json()
    T = JSONweather['main']['temp']
    v = JSONweather['wind']['speed']
    R = JSONweather['main']['humidity']
    conditions = JSONweather['weather'][0]['description']
    e = R / 100 * 6.105 * math.exp((17.27 * T / (237.7 + T)))
    AT = round(T + 0.33 * e - 0.7 * v / 3.6 - 4, 1)
    rain = 'rain' in conditions
    firstLight = JSONweather['sys']['sunrise'] - 25 * 60
    firstLight = datetime.strftime(datetime.fromtimestamp(firstLight), '%H:%M')

    Data = {
        "temp": T,
        "windSpeed": v,
        "humidity": R,
        "conditions": conditions,
        "apparentTemp": AT,
        "rain": rain,
        "clothing": clothing[rain][binning(AT, ATbins)],
        "firstLight": firstLight
    }

    return Data


# advice data
def adviceGathering():
    resp = requests.get(urlAdvice)
    Data = resp.json()['slip']['advice']
    return Data


# finance data
def financeGathering():
    resp = requests.get("https://au.finance.yahoo.com/quote/AAPL/history")
    # Data = resp.json()
    print(resp)


# dog data
def dogGathering():
    resp = requests.get(urlDogs)
    while 'mp4' in resp.json()['url']:
        resp = requests.get(urlDogs)
    return resp.json()['url']


def send_email():
    dataWeather = dataGathering()
    dataAdvice = adviceGathering()
    dataDog = dogGathering()

    today = date.today()
    today = today.strftime("%B %d, %Y")

    with open('emailText.txt') as f:
        lines = f.readlines()
        emailContent = lines[0] + dataWeather['firstLight'] + ' - ' + dataWeather['clothing'] + lines[1] + dataAdvice + lines[2]
        emailContent += dataWeather['conditions'] + lines[3] + str(dataWeather['temp']) + lines[4] + str(dataWeather['humidity']) + lines[5]
        emailContent += str(dataWeather['windSpeed']) + lines[6] + str(dataWeather['apparentTemp']) + lines[7]
        emailContent += '<img src="' + dataDog + '" class="center" width="50%"> </img> <br /></body></html>'

    message = MIMEMultipart("alternative")
    message["Subject"] = "Daily update for " + today
    message["From"] = secret.emailAccount
    message["To"] = receiver_email

    plainText = "Your email client is not receiving HTML emails."

    part1 = MIMEText(plainText, "plain")
    part2 = MIMEText(emailContent, "html")
    message.attach(part1)
    message.attach(part2)

    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login(secret.emailAccount, secret.emailPassword)
        server.sendmail(secret.emailAccount, receiver_email, message.as_string())


# START of program loop #

control()

schedule.every().day.at(sendTime).do(send_email)
f = open("status_file.txt", "w")
f.write("Fail")
f.close()
while True:
    schedule.run_pending()
    time.sleep(15)
    f = open("status_file.txt", "w")
    f.write("Success")
    f.close()
