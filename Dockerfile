FROM python:3.9-slim-buster
WORKDIR /usr/src/app
COPY emailer.py ./
COPY requirements.txt ./
COPY secret.py ./
COPY emailText.txt ./
RUN pip install -r requirements.txt
COPY . .

HEALTHCHECK --interval=15s --timeout=12s --start-period=30s \
	CMD python emailer.py --health

RUN bash -c 'rm -rf /etc/localtime'
RUN bash -c 'ln -s /usr/share/zoneinfo/Australia/Melbourne /etc/localtime'

CMD [ "python", "emailer.py", "&" ]